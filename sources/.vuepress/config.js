const navConfig = require('./nav-config');
// const sideBarConfig = require('./sidebar-config')

module.exports = {
    base:'/analisi-smartedit/',
    dest:'public',
    title: 'WIP Italia - Analisi Smartedit LM',
    description: 'Analisi sviluppo',
    markdown:{
        lineNumbers: true
    },
    extraWatchFiles:[
        '.vuepress/nav-config.js',
        '.vuepress/sidebar-config.js',
    ],
    themeConfig:{
        lastUpdated: 'Last Updated',
        displayAllHeaders:true,
        nav:navConfig,
        sidebar: 'auto',
        sidebarDepth:0
    }

}

