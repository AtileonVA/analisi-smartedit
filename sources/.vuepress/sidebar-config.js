module.exports = [
    {
        title: 'Componenti',   // required
        path: '/componenti',      // optional, link of the title, which should be an absolute path and must exist
        collapsable: false, // optional, defaults to true
        sidebarDepth: 1,    // optional, defaults to 1
        children: [
          '/'
        ]
      }
]