---
title: Analisi Smartedit LM 
next: ./componenti/header-footer
---
# Analisi La Martina + Smartedit (Frontend)

## Premessa 

L'analisi viene effettuata tramite browser inspector dei vari componenti all'interno del sito. In primis il sito si presenta come aspettato. 

### Home: 
<img :src="$withBase('/img/fig-1.png')">

### Menu: 
<img :src="$withBase('/img/fig-2.png')">

Una volta fatto l'accesso allo Smartedit il sito appare invece irregolare. Cosi anche all'interno dello smartedit.
Smartedit(logged):

### Home 
<img :src="$withBase('/img/fig-3.png')">

### Smartedit platform
<img :src="$withBase('/img/fig-4.png')">

::: warning NOTA:
 In alcune pagine compressa quella HOME a volte si presentano notifiche di errori che per la dicitura non è chiaro quanto siano correlate alla attuale visualizzazione del header
::: details Vedere errori
<img :src="$withBase('/img/errors.png')">
:::
:::

## Considerazioni

Considerato il fatto che dovuto al HTML Markup Contract [link TL;DR](https://help.sap.com/viewer/86dd1373053a4c2da8f9885cc9fbe55d/1811/en-US/65aaa344cacb4b389e0689f7e9b3095e.html) lo Smartedit innesca, una volta avviato, l'annidamento dei componenti che vengono dichiarati come:

```java
<cms:component>
<cms:globalSlot>
<cms:pageSlot>
```
nella seguente maniera:

```html
<div class="smartEditComponent" .....>
	<the-original-component/>
</div>
```

ma questo comportamento per quanto è stato rilevato, si da solo per l'utente finale dello Smartedit e non cosi per un utente esterno.

::: warning NOTA:
Lascio spazio ad altre considerazioni da parte di chi ne fará fronte alla inserzione di contenuto, riguardo alle problematiche che questo possa causare nel loro workflow, ad esempio se fosse necessario visualizzare/testare il contenuto al di fuori dello smartedit con la sessione di smartedit attiva
:::

Detto questo, l'intervento mira a risolvere il layout per l'utente di Smartedit ovvero per coloro che andranno ad immettere contenuto per il sito. 

## L'intervento

Attualmente le parti piu rilevanti sono date nel __Header__ del quale si dovrebbe rivedere la struttura all'interno del componente e anche a livello di stili e idem per il footer. 

[Report Header/footer](./componenti/header-footer)

Oltre a questo si vedranno i componenti segnati nel prossimo punto.

## Componenti

I componenti maggiormente usati sui quali si consiglia di intervenire per primo sono:

- Column Wrapper + Column Gallery -> [leggi](./componenti/columns)
- Abstract Fieldset -> [leggi](./componenti/abstract-fieldset)
- Lm product list - il componente non presenta componenti interni (si effettuerá solo un check)


