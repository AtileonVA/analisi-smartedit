---
prev: /
sidebarDepth: 1
---

## Header & footer

A codice sempre per via del HTML Markup contract (d'ora in avanti lo chiameremmo semplicemente '___contract___') dello Smartedit vengono annidati sia i componenti che gli slots.

Andando per parti a seconda di come viene attualmente visto sullo smartedit, prendiamo il blocco principale ('div.nav-container'), separato in due righe (rows).

### Primo row

<img :src="$withBase('/img/header-row-1.png')">

Internamente a questa righa il tag nav contiene una unordered list (ul) tag che regolarmente, annida dei tag li per le voci del menu

<img :src="$withBase('/img/header-row-1-a.png')">

all'interno dello smartedit questa struttura si vede compromessa per via del ___contract___, creando ulteriori blocchi sia per gli slot dichiarati all'interno, e sia per i componenti che vengono immessi all'interno di questi slot

<img :src="$withBase('/img/header-row-1-b.png')">

nella figura sopra si puo notare, partendo dal tag ___'ul'___ come il primo tag ___'li'___ (messa in risalto in basso) venga annidato all'interno di 2 tags 'div', uno per lo 'slot' e quello dopo per il componente che contiene il tag 'li'.

### Secondo row

<img :src="$withBase('/img/header-row-2.png')">

Anche qui come nel primo row, la navigazione (tag nav) viene annidata come da contract, e cosi anche certi tag 'li' 

#### Prima:
<img :src="$withBase('/img/header-row-2-a.png')">

#### Smartedit:
<img :src="$withBase('/img/header-row-2-b.png')">
<img :src="$withBase('/img/header-row-2-c.png')">

## Footer
<img :src="$withBase('/img/footer.png')">

il footer è separato anch'esso in 3 sezioni (slots), nella seguente figura si puo notare l'annidamento delle tre colonne di links all'interno della seconda sezione (FooterLinks).

<img :src="$withBase('/img/footer-a.png')">


## Conclusione

Visto quanto sopra, è evidente che un'alberatura creata in quel modo da parte dello smartedit ci costringe a dover rivedere una gran parte della struttura attuale del ___Header___ e ___Footer___, sia a livello di markup che di stili 

