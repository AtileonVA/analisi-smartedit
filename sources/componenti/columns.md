---
title: Columns
prev: /
---

## Column wrapper 

### Codice

A codice il componente viene annidato all'interno di un tag div con classi:
>.yCmsComponent  .smartEditComponent

Il column wrapper contiene altri 'componenti' parte di un repeater e anche questi vengono annidati all'interno del tag div con la sola classe:
> .yCmsComponent

i repeater contengono le colonne dalle quali bisognerà portare la ereditarietà e ristrutturazione della colonna nel nuovo blocco padre, ovvero il tag div con classe 'yCmsComponent' all'interno del main component (column wrapper) facendo dei check per le varianti unit (sul main component) e cover (su singole colonne)

## Column Gallery

La struttura è simile a quella del column wrapper e l'intervento avrá la stessa procedura. 